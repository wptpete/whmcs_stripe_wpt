<?php

function widget_stripewhmcs($vars) {
	
 	# Required File Includes
	$gatewaymodule = "stripewhmcs"; # Enter your gateway module name here replacing template
	$GATEWAY = getGatewayVariables($gatewaymodule);
	$apiKey = $GATEWAY["APIKey"];
	
	$ch = curl_init(); 
	curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/balance"); 
	curl_setopt($ch, CURLOPT_HEADER, FALSE); 
	//curl_setopt($ch, CURLOPT_NOBODY, TRUE); // remove body 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
	curl_setopt($ch, CURLOPT_USERPWD, $apiKey . ":x");
	$return = curl_exec($ch); 
	//$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
	curl_close($ch); 
			
	$jsonResponse = json_decode($return);
	
	//var_dump($jsonResponse->{"pending"}[0]->{"amount"});
	
    $content = '<p><strong>Pending Balance => </strong>$'. $jsonResponse->{"pending"}[0]->{"amount"}/100 . '</p>';
	$content .= '</br><p><strong>Available Balance => </strong>$'. $jsonResponse->{"available"}[0]->{"amount"}/100 . '</p>';
 
    return array( 'title' => 'STRIPE.COM Balance', 'content' => $content );
 
}
 
add_hook("AdminHomeWidgets",1,"widget_stripewhmcs");

?>
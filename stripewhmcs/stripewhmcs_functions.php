<?php

function stripewhmcs_config() {
    $configarray = array(
     "FriendlyName" => array("Type" => "System", "Value"=>"stripe.com for WHMCS"),
     "APIKey" => array("FriendlyName" => "Production API Key", "Type" => "text", "Size" => "25", ),
     "DevAPIKey" => array("FriendlyName" => "Test API Key", "Type" => "text", "Size" => "25", ),
     "testmode" => array("FriendlyName" => "Test Mode", "Type" => "yesno", "Description" => "Tick this to test", ),
     "callbackid" => array("FriendlyName" => "Callback Security ID", "Type" => "text", "Size" =>"25", "Value"=> md5(uniqid()))
    );
	return $configarray;
}


function stripewhmcs_capture($params) {

    # Invoice Variables
	$invoiceid = $params['invoiceid'];
	$amount = $params['amount']; # Format: ##.##
    $currency = $params['currency']; # Currency Code

    $cardinfo = array(
    	"number" => $params['cardnum'],
    	"exp_month" => substr($params['cardexp'],0,2),
    	"exp_year" => substr($params['cardexp'],2,2),
    	"name" => $params['clientdetails']['firstname'] . " " . $params['clientdetails']['lastname'],
    	"address_line1" => $params['clientdetails']['address1'],
    	"address_line2" => $params['clientdetails']['address2'],
    	"address_city" => $params['clientdetails']['city'],
    	"address_zip" => $params['clientdetails']['postcode'],
    	"address_state" => $params['clientdetails']['state'],
    	"address_country" => $params['clientdetails']['country']
    );

	$metadata = array(
		"invoiceid" => $invoiceid,
		"clientid" =>  $params['clientdetails']['id']
	);

    # Client Variables
	//$email = $params['clientdetails']['email'];
	//$phone = $params['clientdetails']['phonenumber'];

	# Card Details
	//$cardtype = $params['cardtype'];
	//$cardexpiry = $params['cardexp']; # Format: MMYY
	//$cardstart = $params['cardstart']; # Format: MMYY
	//$cardissuenum = $params['cardissuenum'];

	# Perform Transaction Here & Generate $results Array, eg:

    try{
    	Stripe::setApiKey(stripewhmcs_choosekey($params));

    	$charge = Stripe_Charge::create(array(
		  "amount" => $amount*100, //stripe works in pennies
		  "currency" => $currency,
		  "card" => $cardinfo,
		  "description" => "Charge for invoice " . $invoiceid,
		  "metadata" => $metadata
		));



    	//If we are here we have a valid charge
    	return array("status"=>"success","transid"=>$charge["id"],"rawdata"=>$charge);
    	
    }catch(Stripe_CardError $e) {
    	$body = $e->getJsonBody();
		return array("status"=>"declined","rawdata"=>$body['error']);
    } catch (Stripe_AuthenticationError $e) {
  		// Authentication with Stripe's API failed
	  	// (maybe you changed API keys recently)
	  	$body = $e->getJsonBody();
	  	return array("status"=>"error","rawdata"=>$body['error']);
	} catch (Stripe_ApiConnectionError $e) {
	  // Network communication with Stripe failed
		$body = $e->getJsonBody();
		return array("status"=>"error","rawdata"=>$body['error']);
	} catch (Stripe_Error $e) {
		$body = $e->getJsonBody();
	  	return array("status"=>"error","rawdata"=>$body['error']);
	} catch (Exception $e) {
		$body = $e->getJsonBody();
	  	return array("status"=>"error","rawdata"=>$body['error']);
	}

}

function stripewhmcs_refund($params) {

    # Invoice Variables
	$amount = $params['amount']; # Format: ##.##
	$invoiceid = $params['invoiceid'];
   
   $metadata = array(
		"invoiceid" => $invoiceid,
		"clientid" =>  $params['clientdetails']['id']
	);

	try{
    	Stripe::setApiKey(stripewhmcs_choosekey($params));

    	$ch = Stripe_Charge::retrieve($params['transid']);
    	$re = $ch->refunds->create(array(
    		"metadata" => $metadata,
    		"amount" => $amount*100
    		));

    	//If we are here we have a valid charge
    	return array("status"=>"success","transid"=>$re["id"],"rawdata"=>$re);
    	
    }catch(Stripe_CardError $e) {
    	$body = $e->getJsonBody();
		return array("status"=>"declined","rawdata"=>$body['error']);
    } catch (Stripe_AuthenticationError $e) {
  		// Authentication with Stripe's API failed
	  	// (maybe you changed API keys recently)
	  	$body = $e->getJsonBody();
	  	return array("status"=>"error","rawdata"=>$body['error']);
	} catch (Stripe_ApiConnectionError $e) {
	  // Network communication with Stripe failed
		$body = $e->getJsonBody();
		return array("status"=>"error","rawdata"=>$body['error']);
	} catch (Stripe_Error $e) {
		$body = $e->getJsonBody();
	  	return array("status"=>"error","rawdata"=>$body['error']);
	} catch (Exception $e) {
		$body = $e->getJsonBody();
	  	return array("status"=>"error","rawdata"=>$body['error']);
	}
}

function stripewhmcs_choosekey($params){
	if($params['testmode']){
		return $params['DevAPIKey'];
	}else{
		return $params['APIKey'];
	}

}


 

?>
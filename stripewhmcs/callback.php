<?php

# Required File Includes
include("../../../dbconnect.php");
include("../../../includes/functions.php");
include("../../../includes/gatewayfunctions.php");
include("../../../includes/invoicefunctions.php");

$gatewaymodule = "stripewhmcs"; # Enter your gateway module name here replacing template

$GATEWAY = getGatewayVariables($gatewaymodule);
if (!$GATEWAY["type"]) die("Module Not Activated"); # Checks gateway module is active before accepting callback

if($_REQUEST["key"] != $GATEWAY["callbackid"]) die ("Invalid Security Key");

// Retrieve the request's body and parse it as JSON
$input = @file_get_contents("php://input");
$event_json = json_decode($input);

//this is the only event we process
if($event_json->{"type"} == "charge.dispute.created"){
	logTransaction($GATEWAY["name"],$input,"Status Update");
	
	//test trans ch_14DLQr4kkBo7xrxJJn6EdSaq
	
	
	//get the information for this transaction
	
	//whmcs has no easy way to suspend an entire client, we will just 
	$whmcstrans = localAPI("gettransactions",array("transid" => $event_json->{"data"}->{"object"}->{"charge"}),"admin");
	if($whmcstrans["result"] == "success" && $whmcstrans["totalresults"] >= 1){
		//logTransaction($GATEWAY["name"],json_encode($whmcstrans["transactions"]["transaction"][0]["invoiceid"]),"Status Update");
		
		//mark invoice as unpaid and remove amount from invoice
		$updateinvoicevalues = array(
			"invoiceid" => $whmcstrans["transactions"]["transaction"][0]["invoiceid"],
			"status" => "Unpaid"
		);
		$whmcsUpdateInvioce = localAPI("updateinvoice",$updateinvoicevalues,"admin");
		logTransaction($GATEWAY["name"],$whmcsUpdateInvioce,"Status Update");
		
		//remove the amount from the invoice and include the dispute fee
		$addtransvalues = array(
			"description" => "Dispute was open, includes $15.00 dispute fee",
			"transid" => $event_json->{"id"},
			"amountout" => $event_json->{"data"}->{"object"}->{"amount"}/100+15.00,
			"invoiceid" => $whmcstrans["transactions"]["transaction"][0]["invoiceid"],
			"paymentmethod" => $gatewaymodule,
		);
		$whmcsAddTrans = localAPI("addtransaction",$addtransvalues,"admin");
		logTransaction($GATEWAY["name"],$whmcsAddTrans,"Status Update");
		
	}else{
		logTransaction($GATEWAY["name"],"We received a dispute but could not find the invoice to modify: " . $input,"Status Update");
	}
}


# Get Returned Variables - Adjust for Post Variable Names from your Gateway's Documentation
//$status = $_POST["x_response_code"];
//$invoiceid = $_POST["x_invoice_num"];
//$transid = $_POST["x_trans_id"];
//$amount = $_POST["x_amount"];
//$fee = $_POST["x_fee"];

//$invoiceid = checkCbInvoiceID($invoiceid,$GATEWAY["name"]); # Checks invoice ID is a valid invoice number or ends processing

//checkCbTransID($transid); # Checks transaction number isn't already in the database and ends processing if it does

//if ($status=="1") {
    # Successful
//    addInvoicePayment($invoiceid,$transid,$amount,$fee,$gatewaymodule); # Apply Payment to Invoice: invoiceid, transactionid, amount paid, fees, modulename
//	logTransaction($GATEWAY["name"],$_POST,"Successful"); # Save to Gateway Log: name, data array, status
//} else {
	# Unsuccessful
//    logTransaction($GATEWAY["name"],$_POST,"Unsuccessful"); # Save to Gateway Log: name, data array, status
//}

?>
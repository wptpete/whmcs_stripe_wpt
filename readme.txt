Stripe.com integration for WHMCS.
www.mthosting.net


**Installation
copy stripewhmcs and stripewhmcs.php into {WHMCSINSTALL}/modules/gateways/

Enable the gateway in your payment gateways and enter your test and production api keys. Please test the module.


***Usage
Charge and Refund are tested and working.
You can add the call back for chargedispute.open to {WHMCSINSTALL}/modules/gateways/stripewhmcs/callbackup.php?key={key from whmcs} to remove the payment from the invoice and add the $15 fee for the dispute.

**TO DO
finish widget to be similar to the paypal widget